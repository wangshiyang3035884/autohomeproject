/*
 * 名称：图片轮播
 * 说明：带有navi和前后按钮的轮播插件，支持手指拖动。
 */
(function($){
  'use strict';
  var box = $('[data-role="carousel"]'),
      tap = 'touchstart',
      options = {
        autoPlay  : true,
        speed  :  300,
//        setFuncTlme : 2000,
        timeOut : 3000
      },
      opt = box.data('opt');

  if (opt != undefined) {
    var op = opt.replace(/\'/g,'\"');
    try {
      opt = JSON.parse(op);
    } catch(err) {
//      console.warn('wrong to convent json');
      opt = null;
    }
  }
  
  $.extend(options,opt);
  
  var obj = box.find('.slider > ul'),
      o={},
      disX = 0,
      dieX = 0,
      disY = 0,
      dieY = 0,
      n=0,
      t=0,
      speed = 0,
      w = $(window).width(),
      x = 0,
      y = 0,
//      len = 0,
      timer,
      aLi = $('li',obj),
      aImg = $('img',obj);

//  obj.data = {};

  $(window).on('resize',resize);
  
  // 重绘
  function resize(){
    var len = aLi.length;
//      o = obj.data;

    obj.css({ width : len*w + 'px'});
    aLi.css({ width : w });
    aImg.css({ width : w });

    if(speed != 0){
      speed = -t*w;
      obj.css({
        '-webkit-transform'  : 'translate3d('+speed+'px,0,0)'
      });
    }
  };
  // 调用绘制
  resize();
  
  obj.on('touchstart',function(){
    clearInterval(timer);
    var touch = event.targetTouches[0];
    disX = touch.pageX;
    disY = touch.pageY;

    o.S = false;
    o.T = false;

    obj.css({ '-webkit-transition-duration' : '0ms'});
  })
  .on('touchmove',function(event){
    var touch = event.targetTouches[0];
    dieX = touch.pageX;
    dieY = touch.pageY;
    n = dieX - disX;
    x = Math.abs(n);
    y = Math.abs(dieY - disY);

    if(!o.T){
      var S = x < y;
      o.T = true;
      o.S = S;
    }

    if(!o.S){
      event.stopPropagation(); //组织冒泡
      event.preventDefault();  //阻止滚动

      obj.css({
        '-webkit-transform' : 'translate3d('+(n-w*t)+'px,0,0)'
      });
    }
  })
  .on('touchend',function(event){
    if(n > 100){
      t--;
    } else if(n < -100){
      t++;
    }

    if(t <= 0 ){
      t = 0;
    } else if(t >= aLi.length-1){
      t = aLi.length-1;
    }

    move();

    if(options.autoPlay){
      outTime();
    }

    if(!o.S){
      event.preventDefault();  //阻止滚动
    }

  });
  
  function move(){
    var navi = box.find('.navi');
    if (navi.length > 0) {
      $('i',navi).eq(t).addClass('active').siblings().removeClass('active');
    }

    speed = -t*w;

    obj.animate({
      'translate3d' : speed+'px,0,0'
    },options.speed);	
  };

  if(options.autoPlay){
    setMove();
  }

  function setMove(){
    clearInterval(timer);
    timer = setInterval(function(){
      t++;

      if(t < 0 ){
        t = aLi.length-1;
      }
      else if(t > aLi.length-1){
        t = 0;
      }

      move();
    },options.timeOut)	
  };
    
  function outTime(){
    setTimeout(function(){
      setMove();
    },options.timeOut)
  };
  
  box.find('.navi').find('.next').on(tap,function(){
    clearInterval(timer);
    t++;

    if(t > aLi.length-1){
      t = 0;
    }

    move();
    outTime();
  });

  box.find('.navi').find('.prev').on(tap,function(){
    clearInterval(timer);
    t--;

    if(t < 0){
      t = aLi.length-1;
    }

    move();
    outTime();
  });
  
})(Zepto)

