;(function(s,c){
  'use strict';
  var i = new Image(),
      tap = 'click',
      urlclick = '//sou.autohome.com.cn/stats/Click.ashx?',
      urlsubmit = '//sou.autohome.com.cn/stats/Search.ashx?',
      datakey = 'click'; // data-xxx的xxx
  
  // 页面提交搜索时
  i.src = urlsubmit + toQuery(s);
  
  // 代理页面中所有的a
  $(document).on(tap,'a',function(e){
    var $this = $(this);
    c.x = e.pageX;
    c.y = e.pageY;
    c.uri = this.href;
    c.anchor = $this.data('title') === undefined ? $this.html() : $this.data('title');
//    c.anchor = encodeURIComponent(c.anchor);
    c.data = $this.data('click');
    if (!$this.data('ignore') && $this.data(datakey) !== '') {
      var ii = new Image();
      ii.src = urlclick + toQuery(c);
    }
  })
  
  // 对象转QueryString
  function toQuery(obj) {
    var s = "";
    for (i in obj) {
      s += i + "=" + encodeURIComponent(obj[i]) + "&";
    }
    return s;
  }
  
})(searchObj,clickObj)