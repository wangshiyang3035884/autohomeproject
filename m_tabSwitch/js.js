/*
 * 名称：Tab切换
 * 说明：需要严格遵循标签结构 (ul>li*n) + (div>div*n)
 */
(function($){
  'use strict';
  var tab = $('[data-role=tabSwitch]'),
      idx = 0,
      con = tab.next();
  tab.on('click',function(e){
    if (e && e.target.tagName === 'LI') {
      var li = $(e.target),
          idx = li.index();
      li.addClass('active').siblings().removeClass('active');
      con.find('div').eq(idx).show().siblings().hide();
    }
  });
})(Zepto)