// 定义需要进行压缩的js组件
var moduleName = [
  './m_backTop/js.js',      // 返回顶部
  './m_dropdown/js.js',     // 下拉菜单
  './m_lazyload/js.js',     // 延迟加载lazyload
  './m_picSlider/js.js',    // 图片轮播
  './m_popbox/js.js',       // 弹层
  './m_search_bar/js.js',   // 搜索条
  './m_tabSwitch/js.js',    // Tab切换
]

var gulp = require('gulp');

var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

gulp.task('scripts', function() {
  gulp.src(moduleName)
    .pipe(concat("mass.js"))
    .pipe(gulp.dest('./src/'))
});

gulp.task('compress', function() {
  gulp.src('./src/mass.js')
    .pipe(uglify({outSourceMap: true}))
    .pipe(gulp.dest('./build'))
});
  
gulp.task('default', ['scripts','compress']);