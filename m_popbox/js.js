/*
 * 名称：弹出层插件
 * 说明：可以指定是否带有mask。
 */
(function($){
  'use strict';
  
  $('[data-role="popbox"]').on('click',function(){
    showPop('con','tit',true);
  });
  
  function showPop(content,title,mask) {
    var pop = $('.popbox').empty();
    if (title != null) {
      $('<h3>').text(title).appendTo(pop);
    }
    $('<p>').text(content).appendTo(pop);
    
    if (mask) {
      $('<div id="mask">')
        .css({
          "width":"100%",
          "height":"100%",
          "position":"fixed",
          "top":"0",
          "left":"0",
          "background":"black",
          "opacity":"0.6",
          "z-index":"50"
          })
        .appendTo('body')
        .on('click',function(){
          $(this).detach();
          pop.hide();
        });
    }
    
    pop.show();
  }
  
})(Zepto)