/*
 * 名称：回到顶部
 * 说明：滚动超过1屏，出现回到顶部的按钮
 */
(function($){
  'use strict'
  var el = $('[data-role="backTop"]'),
      win = $(window),
      timer;
  $(window).scroll(function(){
    if ( timer ) clearTimeout(timer);
    timer = setTimeout(function(){
      if (win.scrollTop() > win.height()) {
        el.show();
      } else {
        el.hide();
      }
    }, 100);
  });
  el.on('click',function(){
    window.scrollTo(0,0);
  });
})(Zepto)