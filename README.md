# 汽车之家MASS
***
### 说明
MASS框架，JS组件部分。

各常用JS组件的源码，DEMO和使用方法。

### 特性
* 每个模块单独开发，附带相应的HTML和JS代码部分
* 代码使用了JS的严格模式的匿名函数执行方式构造
* 基于Zepto单独开发。在使用下列JS控件前，请保证页面中已经引入了Zepto库。地址：http://x.autoimg.cn/as/static/js/zepto-1.2.min.js
* 采用data-role属性作为选择器锚，不干扰html标签中的class部分
* 采用gulpjs自动化构建工具进行项目构建
* 代码压缩使用uglifyjs v2
* 做到逻辑（JS）与展示（CSS）分离

### 结构
/src/ 目录 mass.js 合并后源文件。

/build/ 目录 mass.js 合并后并使用uglifyJS压缩版本。**线上引用本版本。**