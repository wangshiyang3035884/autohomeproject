/*
 * 名称：延迟加载
 * 说明：data-src属性控制图片的延迟加载是否开启。
 * 注意：原生，不需要Zepto
 */
(function(){
  'use strict';
  var store = [], // 图片储存器
      offset = 0, // 提前100像素开始加载
      timeout = 250, // 异步的timeout
      poll; // 定时器
  
  // 判断当前这个img元素是否将要显示
  function _inView(element) {
    var coords = element.getBoundingClientRect();
    return ((coords.top >= 0 && coords.left >= 0 && coords.top) <= (window.innerHeight || document.documentElement.clientHeight) + offset);
  }
  
  // 异步获取图片
  function _throttle () {
    clearTimeout(poll);
    poll = setTimeout(_pollImages, timeout);
  }
  
  // 获取图片
  function _pollImages () {
    var len = store.length;
    if (len > 0) {
      for (var i = 0; i < len; i++) {
        var self = store[i];
        if (self && _inView(self)) {
//          self.src = self.getAttribute('data-src');
          self.src = self.dataset.src;
          store.splice(i, 1);
          len = store.length;
          i--;
        }
      }
    } else {
      window.removeEventListener('scroll',_throttle);
//      $(window).off('scroll',_throttle);
      clearTimeout(poll);
    }
  }

  var nodes = document.querySelectorAll('img[data-src]');
//  var nodes = $('img[data-src]');

  for (var i = 0; i < nodes.length; i++) {
    store.push(nodes[i]);
  }
  _pollImages();
  window.addEventListener('scroll',_throttle,false);
//  $(window).on('scroll',_throttle);  

})()