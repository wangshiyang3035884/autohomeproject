/*
 * 名称：下拉菜单
 * 说明：点击触发DOM，显示/隐藏紧跟着的div
 */
(function($){
  'use strict';
  var box = $('[data-role="dropdown"]'),
      tap = 'click';
  box.on(tap, function(e) {
    $(this).next().toggle();
  })
})(Zepto)