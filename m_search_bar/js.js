/*
 * 名称：搜索条插件
 * 说明：JSONP获取搜索结果，提示层列表，带有统计代码。
 */
;(function () {
  'use strict';
  function getCookie(name) {
    var tmp,reg=new RegExp("(?:^| )"+name+"=([^;]*)(?:;|$)","gi");
    return (tmp=reg.exec(document.cookie))?(unescape(tmp[1])):1;
  }
                                              
  function setCookie(name, value) {
//    console.log("set cookie");
    var nowDate = new Date();
    nowDate.setTime(nowDate.getTime() + 24*60*60*1000);
//    document.cookie = name + "=" + escape(value) + "; expires=" + nowDate.toGMTString() +  "; path=/; domain=autohome.com.cn;";
//测试用，不指定域名    
    document.cookie = name + "=" + escape(value) + "; expires=" + nowDate.toGMTString() +  "; path=/; domain=;";
  }
/**
 * 对Local Storage操作，设置和返回搜索历史记录
 */
/*  
var SearchHistory = {
  
  lskey : "sh", // localstroage的key
  ls : null,
//  throughURL : '//m.autohome.com.cn/', // 直达链接跳转前缀 url/直达id
  
  getHistory : function() {
    this.ls = window.localStorage ? window.localStorage : null;
    // 初始化对象，如果没有这个key和value，设置个空的对象
    var obj = {
          // 综合文章
          a : {
            // 直达 name@id@bbstype
            t : [
              ""
            ],
            // keywords
            k : ""
          },
          // 论坛
          f : {
            t : [
              ""
            ],
            k : ""
          }
        };
    if (this.ls != null) {
      if (this.ls.getItem(this.lskey) == null) {
        this.setHistory(obj);
        return obj;
      } else {
        return JSON.parse(this.ls.getItem(this.lskey));
      }
    } else {
      return false;
    }
  },
  setHistory : function(data) {
//    console.log('yes')
    if (this.ls != null && data) {
      this.ls.setItem(this.lskey,JSON.stringify(data));
    } else {
      return false;
    }
  }
}
*/
var SearchHistory = {
  
  key : "searchhistory", // localstroage的key
  emptydata : {
          // 综合文章
          a : {
            // 直达 name@id@bbstype
            t : [
              ""
            ],
            // keywords
            k : ""
          },
          // 论坛
          f : {
            t : [
              ""
            ],
            k : ""
          }
        },
//  ls : null,
//  throughURL : '//m.autohome.com.cn/', // 直达链接跳转前缀 url/直达id
  
  getHistory : function() {
//    this.ls = window.localStorage ? window.localStorage : null;
    // 初始化对象，如果没有这个key和value，设置个空的对象
//    var obj = ;
    var data = getCookie(this.key);
    if (data === 1 || data === "") {
      this.clearHistory();
      return this.emptydata;
    } else {
      return JSON.parse(data);
    }
  },
  setHistory : function(data) {
//    console.log('yes')
    setCookie(this.key,JSON.stringify(data))
  },
  clearHistory:function() {
//    console.log('clear');
//    this.setHistory(this.emptydata);
    setCookie(this.key,"");
  }
}

  var tap = 'click',
      hideClass = 'w-search-hide',
      $search = $('.w-search'),
      inputString = '.w-search-input-text',
      popboxString = '.w-search-pop-box',
      spacerString = '#searchBar_spacer',
      lsdata = SearchHistory.getHistory(),
      iconAdd = '<span><i class="w-search-icon"></i></span>',
//      keywordType = 1,
      isForum = 0, // 当前类型是否是论坛
      currentBar = null,
      throughURL = '//m.autohome.com.cn/', // 直达链接跳转前缀 url/直达id
      throughURLForum = '//club.m.autohome.com.cn/bbs/forum-';
//      keywordURL = 'http://sou.autohome.com.cn/stats/SearchWord.ashx?word='; // 关键字链接跳转前缀 url?关键字
  
  // 事件绑定
  $search
    // 搜索分类弹层切换
    .on(tap,'.w-search-tab-btn',function(e){
      $(this).next().toggleClass(hideClass);
//      var pop = currentBar.find(popboxString).toggleClass(hideClass);
//      var pop = $(this).next();
//      // 关闭已有的提示弹层
//      $search.find(popboxString).addClass(hideClass);
//      pop.removeClass(hideClass);
//      setTimeout(function(){
//        $(document).one(tap,function(){
//        pop.addClass(hideClass);
//      })},10);
      e.preventDefault();
    })
    // 搜索分类点击切换文本
    .on(tap,'.w-search-tab-pop a',categoryBtn)
    // 浮层的关闭按钮
    .on(tap,'.w-search-pop-bar a',function(e){
//      console.log(1);
//      $(this).closest(popboxString).addClass(hideClass);
      scrollBack();
      // 给UC执行FIX
      !!window.fixuc && window.fixuc();
      setTimeout(closeLayer,250);
      e.preventDefault();
    })
    // 输入框获得焦点，打开浮层
    .on('focus',inputString,showHistory)
    // 失去焦点，去掉占位div
//    .on('blur',inputString,function () {
//      $(spacerString).detach();
//    })
    // 输入框输入内容，更新浮层内容
    .on('input',inputString,updatePop)
    // 输入框右侧清空按钮，点击清空输入框
    .on(tap,'.w-search-input-clear',clearInput)
    // 跳转前，保存在本地存储中，直达
    .on(tap,'.w-search-pop-through a',function (e) {
      var $tar = $(e.target);
      saveThrough($tar.text(),$tar.data('id'),$tar.data('type'));
      // 添加监控，直达
      sendAna($tar.text());
//      currentBar.find(popboxString).addClass(hideClass);
      closeLayer();
//      e.preventDefault();
    })
    // 跳转前，保存在本地存储中，综合搜索
    .on(tap,'.w-search-pop-normal a', function (e) {
//      var val = e.target.innerText;
      var link = e.target;
      if (link.nodeName.toLocaleLowerCase() === 'b') {
        link = link.parentNode;
      }
      var val = $(link).text();
      if (val === '') {
        return ;
      }
      saveKeyword(val);
      sendAna(val);
      closeLayer();
      // 添加监控，联想词
//      return false;
      currentBar.find(inputString).val(val);
      currentBar.find('form')[0].submit();
//      e.stopPropagation();
      e.preventDefault();
    })
    // 点击提交，提交表单
    .on(tap,'.w-search-btn', function(e){
      currentBar = $(e.target).closest('.w-search');
      var val = currentBar.find(inputString).val();
      if (val === '') {
        return ;
      }
      saveKeyword(val);
      closeLayer();
      $(this).closest('form')[0].submit();
    })
    .on('submit','form', function(e){
//      console.log('submit');
      saveKeyword(currentBar.find(inputString).val());
      closeLayer();
    })
    // 点击清空历史记录
    .on(tap,'.w-search-pop-clear',function(){
//      console.log('clear btn is pressed');
      SearchHistory.clearHistory();
      currentBar.find('.w-search-pop-box').addClass(hideClass);
      currentBar.find('.w-search-pop-through').remove();
      lsdata = null;
    })
    // 点击加好，关键字上input
    .on(tap,'dd span',function(){
      var txt = $(this).prev().text();
//      currentBar.find(inputString)[0].focus();
      currentBar.find(inputString).val(txt)[0].focus();
    })

  // 搜索条置顶
  function scrollBack() {
    setTimeout(function(){window.scrollTo(0,currentBar.offset().top-10);},50);
  }
  
  // 显示历史记录
  function showHistory (e) {
//    console.log('show History');
    var htm = '',val = '';
    // 记住当前操作的上Bar还是下Bar
    if (e) {
      val = $(e.target).val();
      currentBar = $(e.target).closest('.w-search');
    }
    // 判断当前是否为论坛 isForum
    // 蔡超 北京车展 201403180426 isForum不是0，1了，改为标示文章、综合、论坛、视频？
    var ac = currentBar.find('form').attr('action');
//    console.debug(ac.indexOf('shipin')>-1);
    if (ac.indexOf('wenzhang')>-1) {
      isForum = 1
    } else if (ac.indexOf('luntan')>-1) {
      isForum = 2
    } else if (ac.indexOf('shipin')>-1) {
      isForum = 3
    }
//    isForum = currentBar.find('form').attr('action').indexOf('luntan') > -1;

    // 搜索条置顶
    scrollBack();
    if (val !== '') {
      // input有值，并且弹层内有内容，弹出已有弹层
      updatePop();
    } else {
      var wrapper = currentBar.find(popboxString),
      htm = getPopContent(getThrough(),getKeyword(),true);
      if (htm != '') {
        wrapper.html(htm).removeClass(hideClass);
      } else {
        wrapper.addClass(hideClass);
      }
    }
    // 创建占位div，判断当前触发点是否在屏幕底部
    if ($(document).height() - currentBar.offset().top < $(window).height()) {
      if ($(spacerString).length === 0) {
        $('<div id="searchBar_spacer">').css('height',380).appendTo($('body'));
      }
    }
  }
  
  // 拼装联想HTML，<a href="abc">abc</a><a href="abc">abc</a>
  function getKeyword() {
//    console.log('getKeyword');
    var list = [],
        str = '';
    if (lsdata != null) {
      
      // 判断当前选择的类别是否是论坛？
      if (isForum===2) {
        list = lsdata.f.k.split(',');
      } else {       
        list = lsdata.a.k.split(',');
      }
      list.forEach(function(item){
        if (item!='') {
          str += '<dd><a href="' +item+ '">'+item+'</a>' + iconAdd;
        }
      });
    }
    return str;
  }
  
  // 拼装直达HTML，<a href="http://m.autohome.com.cn/id1/">name1</a><a href="http://m.autohome.com.cn/id2/">name2</a><a href="http://m.autohome.com.cn/id3/">name3</a> 
  function getThrough() {
//    console.log('getThrough');
    var list = [],
        arr = [],
        str = '',
        bbstype = 'c';
    if (lsdata != null) {
      
      // 判断当前选择的类别是否是论坛？
      if (isForum===2) {
        list = lsdata.f.t;
      } else {       
        list = lsdata.a.t;
      }
      
      list.forEach(function(item){
        if (item!='') {
          arr = item.split('@');
          if (isForum===2) {
            if (arr[2] !== '') {
              bbstype = arr[2]
            }
            str += '<dd><a href="' + throughURLForum + bbstype + '-' + arr[1] + '-1.html" data-id="'+arr[1]+'" data-type="' +bbstype+ '">'+arr[0]+'</a>' + iconAdd;
          } else {
            str += '<dd><a href="' + throughURL + arr[1] + '/" data-id="'+arr[1]+'">'+arr[0]+'</a>' + iconAdd;
          }
        }
      });
    }
    return str;
  }
  
  // 拼装弹层with历史记录
  // @param: isHistory 是否是历史记录层
  function getPopContent(throughList,keywordList,isHistory) {
    if (throughList === '' && keywordList === '') {
      return '';
    }
    
    var htm = '',
        txtThrough = '',
        txtKeyword = '';
    
    if (isForum===2) {
      txtThrough = txtKeyword = '论坛';
    } else if (isForum===3) {
      txtThrough = txtKeyword = '视频';
    } else {
      txtThrough = '车系';
      txtKeyword = '综合'
    }
    
    if (throughList !== '') {
      htm += '<dl class="w-search-pop-through">';
      htm += '<dt><i class="w-search-icon"></i>直达' +txtThrough+ '</dt>';
//      htm += '<dd>';
      htm += throughList;// 这里循环直达
//      htm += '</dd>';
      htm += '</dl>';
    }
    if (keywordList !== '') {
      htm += '<div class="w-search-pop-divi"></div>';
      htm += '<dl class="w-search-pop-normal">';
      htm += '<dt><i class="w-search-icon"></i>' +txtKeyword+ '搜索</dt>';
//      htm += '<dd>';
      htm += keywordList;// 这里循环关键字
//      htm += '</dd>';
      htm += '</dl>';
    }
    htm += '<div class="w-search-pop-bar">';
    // 只在显示历史记录时，出现清除选项
    if (isHistory) {
      htm += '<a class="w-search-pop-clear">清除历史记录</a>';
    }
    htm += '<a class="w-search-pop-close">关闭</a>';
    htm += '</div>';
    htm += '</div>';
    
    return htm;
  }
  
  // 更新弹层内容，当输入时
  function updatePop() {
//    console.log('updatePop');
    
    var $input = this !== undefined ? $(this) : currentBar.find(inputString),
        inputValue = $input.val(),
        // AJAX接口的URL
//        uriprefix = '//sou.m.autohome.com.cn/Api/',
        uriprefix = 'http://t0.sou.m.autohome.com.cn/Api/',
        articleUrl = uriprefix + 'Suggestword/search',
        videoUrl = uriprefix + 'VideoSuggestword/search', // 蔡超 北京车展 20140318 添加视频接口地址
        forumUrl = uriprefix + 'TopicSuggestword/search';
    
    // 文本框清空按钮（居右的叉子）
    if (inputValue !== '') {
      $input.parent().find('.w-search-input-clear').removeClass(hideClass)
    } else {
      $input.parent().find('.w-search-input-clear').addClass(hideClass);
      clearInput();
      return;
    }
    
    var ajaxurl = articleUrl;
    if (isForum===2) {
      ajaxurl = forumUrl
    } else if (isForum===3) {
      ajaxurl = videoUrl
    }
//    console.debug(ajaxurl);
    // 这里要写ajax，更新弹层
    $.ajax({
      url: ajaxurl,
      data: {
        q: inputValue
      },
      dataType: 'jsonp',
      success: function (data) {
        if (data.returncode ===0) {
          showResult(data.result,inputValue);
        } else {
          console.error('interface data error：' + data.message);
        }
      }
    });
  }
  
  // 输入框右侧清空按钮（居右叉子）点击事件，清空输入值，更新弹层内容为历史记录
  function clearInput(e){
//    console.log('clearInput');
    $(this).addClass(hideClass).parent().find('input').eq(0).val('');
    if (currentBar===null) {
      currentBar = $(e.target).closest('.w-search');
    }
    currentBar.find('.w-search-pop-through,.w-search-pop-divi,.w-search-pop-normal').remove();
    showHistory();
//    currentBar.find(inputString).trigger('focus');
    currentBar.find(inputString)[0].focus();
    if (e) {
      e.stopPropagation();
      e.preventDefault();
    }
  }
  
  // 出弹层，ajax回调函数
  function showResult(list,kw) {
//    console.log('showResult');
    var input = currentBar.find('input'),
        htm = '';

    if (list.length>0){
      var strThrough='',strKeyword='';
      list.forEach(function(item){
        if (item.wordtype === 3) {
          if (isForum===2) {
            strThrough += '<dd><a href="' + throughURLForum + item.bbs + '-' + item.wordid + '-1.html" data-id="'+item.wordid+'" data-type="' +item.bbs+ '">'+item.name+'</a>' + iconAdd;
          } else {
            strThrough += '<dd><a href="' + throughURL + item.wordid + '/" data-id="'+item.wordid+'">'+item.name+'</a>' + iconAdd;
          }
        } else {
          strKeyword += '<dd><a href="' + item.name + '">'+item.name.replace(kw,'<b>'+kw+'</b>')+'</a><span><i class="w-search-icon"></i></span>';
        }
      });
      
      htm = getPopContent(strThrough,strKeyword);
      $(popboxString,currentBar).html(htm).removeClass(hideClass);
    } else {
      $(popboxString,currentBar).addClass(hideClass);
    }
  }
  
  // 搜索条左侧的综合/文章/论坛按钮切换，包括INPUT有值时的表单提交
  function categoryBtn (e){
    // 更新文案为点击的文案
    $('.w-search-tab-btn',$search).html($(this).text()+'<i></i>');

    // 改变form的action
    var $this = $(this),
        idx = $this.index(),
        form = $this.closest('form'),
        type = '',
        action = form.data('action');
    if (idx === 0) {
      type = 'zonghe';
      isForum = 0;
    } else if (idx === 1) {
      type = 'wenzhang';
      isForum = 1;
    } else if (idx === 2) {
      type = 'luntan';
      // 更新全局变量ifForum为真假
      isForum = 2;
    } else if (idx === 3) {
      type = 'shipin';
      // 更新全局变量ifForum为真假
      isForum = 3;
    }
    form.attr('action',action+type);
    
    if (form.find(inputString).val()!=='') {
      form[0].submit();
    } else {
      $this.parent().addClass(hideClass);
    }
    $(this).closest(popboxString).addClass(hideClass);
    e.preventDefault();
  }
  
  // 保存点击联想词到ls中
  function saveKeyword(val){
    var keys = [],str='';
    if (isForum===2) {
      str = lsdata.f.k;
    } else {
      str = lsdata.a.k;
    }

    if (str.indexOf(val)>-1) {
//      console.log('有重复');
      return 
    } else {
      keys = str.split(',');
//    return false;
      
      if (keys.length === 0) {
        keys.push(val);
      } else {
        keys.unshift(val);
        if (keys.length>3) {
          keys.pop(3);
        }
      }
      
      if (isForum===2) {
        lsdata.f.k = keys.join(',');
      } else {
        lsdata.a.k = keys.join(',');
      }
      SearchHistory.setHistory(lsdata);
    }
    
  }
  
  // 保存点击直达到ls中
  function saveThrough(name,id,type){
//    console.log(name,id,type)
    var keys = [];
    
    if (type === undefined) {
      type = '';
    }
    
    // 判断当前选择的类别是否是论坛？
    if (isForum===2) {
      keys = lsdata.f.t;
    } else {
      keys = lsdata.a.t;
    }
    
    if (keys.join(',').indexOf(id) > -1) {
//      console.log('有重复');
      return 
    } else {
      keys.unshift(name+'@'+id+'@'+type);
      if (keys.length>3) {
        keys.pop(3);
      }
      
      if (isForum===2) {
        lsdata.f.t = keys;
      } else {
        lsdata.a.t = keys;
      }
      SearchHistory.setHistory(lsdata);
    }
  }
  
  // 关闭弹层
  function closeLayer(){
    currentBar.find(popboxString).addClass(hideClass);
    $(spacerString) && $(spacerString).detach();
  }
  
  // 添加点击监控
  function sendAna (word) {
    var i = new Image(),
        u = '//sou.autohome.com.cn/stats/TipWordSouLog.ashx?word='+word+'&type=',
        t = currentBar.find('.w-search-tab-btn').text(),
        x = 30; // type。 30综合，31文章，32论坛，33视频
        
    if (t==='文章') {
      x = 31;
    } else if (t==='论坛') {
      x = 32;
    } else if (t==='视频') {
      x = 33;
    }
    u += x;
//    console.log(u);
    i.src = u;
    return ; 
  }
})();